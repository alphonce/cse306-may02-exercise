#include <stdio.h>
#include <string.h>
#include "List.h"

void display(int x) {
  printf("%d as a string is \"%s\"\n",x,int2string(x));
}

int main(int argc, char * argv[]) {
  char * x;
  *x = 123;
  display(12345);
  display(-12);
  display(-999);
  return 0;
}
